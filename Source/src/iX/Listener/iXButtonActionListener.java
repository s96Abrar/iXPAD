/*
 * iXPAD is a simple text editor with bookmark, syntax highlighting, recent activity, spell check
 * 
 * Copyright (C) 2019  Abrar
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package iX.Listener;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import iX.Windows.iXPAD;

/**
 * @author abrar
 *
 */
public class iXButtonActionListener implements ActionListener {

	private iXPAD ixPAD;

	public iXButtonActionListener(Component parent) {
		if (parent instanceof iXPAD) {
			ixPAD = (iXPAD) parent;
		} else {
			System.out.println("iXPAD : Parent not found...");
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "Open") {
			ixPAD.createTab();
			String filePath = ixPAD.getiXEditor().openFile();
			if (filePath != null) {
				ixPAD.getTabPane().updateTitle(new File(filePath).getName());
			} else {
				System.out.println("Can not open file " + filePath);
				ixPAD.closeTab();
			}
		} else if (e.getActionCommand() == "New Page") {
			ixPAD.createTab();
		} else if (e.getActionCommand() == "Save") {
			String filePath = ixPAD.getiXEditor().saveFile();
			if (filePath != null) {
				ixPAD.getTabPane().updateTitle(new File(filePath).getName());
			}
		} else if (e.getActionCommand() == "Save As") {
			String filePath = ixPAD.getiXEditor().saveAsFile();
			if (filePath != null) {
				ixPAD.getTabPane().updateTitle(new File(filePath).getName());
			}
		} else if (e.getActionCommand() == "Copy") {
			ixPAD.getiXEditor().copy();
		} else if (e.getActionCommand() == "Paste") {
			ixPAD.getiXEditor().paste();
		} else if (e.getActionCommand() == "Cut") {
			ixPAD.getiXEditor().cut();
		} else if (e.getActionCommand() == "Undo") {
			ixPAD.getiXEditor().undo();
		} else if (e.getActionCommand() == "Redo") {
			ixPAD.getiXEditor().redo();
		} else if (e.getActionCommand() == "Search") {
			ixPAD.getiXEditor().search();
		} else if (e.getActionCommand() == "Activity") {
			ixPAD.showActivity();
		} else if (e.getActionCommand() == "Pin It") {
			ixPAD.pinIt();
		} else if (e.getActionCommand() == "Pin View") {
			ixPAD.showPinView();
		} else if (e.getActionCommand() == "Settings") {
			ixPAD.showSettings();
		} else if (e.getActionCommand() == "About") {
			ixPAD.showAbout();
		} else if (e.getActionCommand() == "Check Duplicity") {
			ixPAD.showDiff();
		} else {
			System.out.println("iXPAD : No action found for \"" + e.getActionCommand() + "\"");
		}
	}

}
